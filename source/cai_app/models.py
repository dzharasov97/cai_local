from django.db import models


class VirtualMachine(models.Model):
    name = models.CharField(
        max_length=150,
        verbose_name='Наименование',
        null=False,
        blank=False
    )
    operating_system = models.CharField(
        max_length=150,
        verbose_name='Операционная система',
        null=False,
        blank=False
    )
    ip = models.CharField(
        max_length=150,
        verbose_name='IP',
        null=False,
        blank=False
    )
    physical_server = models.ForeignKey(
        to='cai_app.PhysicalServer',
        on_delete=models.CASCADE,
        related_name='physical_servers',
        verbose_name='Физический сервер'
    )
    application = models.TextField(
        max_length=300,
        verbose_name='Приложение',
        null=False,
        blank=True
    )
    note = models.TextField(
        max_length=300,
        verbose_name='Примечание',
        null=False,
        blank=True
    )

    def __str__(self):
        return f"{self.name} - {self.operating_system} - {self.ip}"

    class Meta:
        verbose_name = 'Виртуальная машина'
        verbose_name_plural = 'Виртуальные машины'


class PhysicalServer(models.Model):
    name = models.CharField(
        max_length=150,
        verbose_name='Наименование',
        null=False,
        blank=False
    )
    operating_system = models.CharField(
        max_length=150,
        verbose_name='Операционная система',
        null=False,
        blank=False
    )
    ip = models.CharField(
        max_length=150,
        verbose_name='IP',
        null=False,
        blank=False
    )

    def __str__(self):
        return f"{self.name} - {self.operating_system} - {self.ip}"

    class Meta:
        verbose_name = 'Физический сервер'
        verbose_name_plural = 'Физические сервера'
