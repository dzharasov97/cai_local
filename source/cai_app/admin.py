from django.contrib import admin
from cai_app.models import PhysicalServer, VirtualMachine

admin.site.register(PhysicalServer)

admin.site.register(VirtualMachine)
