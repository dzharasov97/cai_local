from django.urls import path
from cai_app.views import IndexView, PhysicalServerAddView, \
    VirtualMachineAddView, PhysicalServerUpdateView, VirtualMachineUpdateView


urlpatterns = [
    path('', IndexView.as_view(), name='index'),
    path('add/server', PhysicalServerAddView.as_view(), name='add_server'),
    path('add/virtual/machine', VirtualMachineAddView.as_view(), name='add_virtual'),
    path('update/server/<int:pk>/', PhysicalServerUpdateView.as_view(), name='update_server'),
    path('update/virtual/<int:pk>/', VirtualMachineUpdateView.as_view(), name='update_virtual')
]
