from django import forms
from cai_app.models import PhysicalServer, VirtualMachine


class PhysicalServerForm(forms.ModelForm):

    class Meta:
        model = PhysicalServer
        fields = ('name', 'operating_system', 'ip')

class VirtualMachineForm(forms.ModelForm):
    physical_server = forms.ModelChoiceField(required=True, label='Физический сервер', queryset=PhysicalServer.objects.all(), initial=[0])

    class Meta:
        model = VirtualMachine
        fields = ('name', 'operating_system', 'ip', 'physical_server', 'application', 'note')

