from django.views.generic import ListView, CreateView, UpdateView
from cai_app.models import PhysicalServer, VirtualMachine
from cai_app.forms import PhysicalServerForm, VirtualMachineForm
from django.urls import reverse


class IndexView(ListView):
    template_name = 'index.html'
    context_object_name = 'virtual_machines'
    model = VirtualMachine

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(IndexView, self).get_context_data(object_list=object_list, **kwargs)
        context['physical_servers'] = PhysicalServer.objects.all()
        return context


class PhysicalServerAddView(CreateView):
    model = PhysicalServer
    template_name = 'add_server.html'
    form_class = PhysicalServerForm

    def get_success_url(self):
        return reverse('index')


class VirtualMachineAddView(CreateView):
    model = VirtualMachine
    template_name = 'add_virtual.html'
    form_class = VirtualMachineForm

    def get_success_url(self):
        return reverse('index')


class PhysicalServerUpdateView(UpdateView):
    template_name = 'update_server.html'
    form_class = PhysicalServerForm
    model = PhysicalServer
    context_object_name = 'physical_server'

    def get_success_url(self):
        return reverse('index')


class VirtualMachineUpdateView(UpdateView):
    template_name = 'update_virtual.html'
    form_class = VirtualMachineForm
    model = VirtualMachine
    context_object_name = 'virtual_machine'

    def get_success_url(self):
        return reverse('index')
